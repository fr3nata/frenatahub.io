LUCIO: Away! let's go learn the truth of it.

Yet he's gentle, never
	schooled and yet learned, full of noble device, of
		all sorts enchantingly beloved

And you all know, security
	Is mortals' chiefest enemy.


